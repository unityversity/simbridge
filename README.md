# SIMBRIDGE is a hackerspace simulator you can hack to simulate your own hackerspaces!
* Simbridge simulates activity stations as modular prefabs that can be placed within your own customized floorplans.
* The example hackerspace is Noisebridge (http://noisebridge.net/simbridge) where Simbridge was created.

The Sketchup modeled version
-----------------------------

[<img src="https://gamebridgeu.files.wordpress.com/2016/09/simbridge.jpg?w=1000">](https://gamebridgeu.wordpress.com/2016/10/21/simbridge/)

The Matterport 3D scanned version
----------------------------------
* You can view and download the Matterport models directly
* [Noisebridge Matterport scan](https://my.matterport.com/models/k85evX2qUCg) 750MB+ of textures and the OBJ import may crash with only 16GB of RAM.
* [Noisebridge Woodshop Matterport scan](https://my.matterport.com/models/PnRkRoACbYV)

[<img src="https://gamebridgeu.files.wordpress.com/2016/10/simbridge-matterport-scan.jpg?w=1000">](https://gamebridgeu.wordpress.com/2016/10/21/simbridge/)

TECH
------------
Simbridge is developed with Unity, a free game engine available for Mac/Windows/Linux that can build for 21 platforms including HTML5 WebGL.
* You can develop for it by installing Unity and cloning the repo to try it or forking the repo to start your own version.
* http://unity3d.com

WAYS TO CONTRIBUTE
------------
If you like Simbridge:
* consider donating to the Simbridge project at Noisebridge: https://donate.noisebridge.net/projects/simbridge
* improve and create new activity station prefabs
* improve on the Noisebridge space scene by capturing new details and adding them
* create new hackerspace scenes by forking the project and designing your level in Blender, Sketchup, Saber CSG, etc.
* introduce new people to hackerspaces by sharing SimBridge with them

TO DO
-----------

LIVE WEB DEMO
----------
We will post an HTML5 web player build of Simbridge soon so you can try it and see how you can publish the simulator builds on your own hackerspace sites.

ACTIVITY STATIONS
---------
We have some work in progress on a few stations that need completing:
* Circuit hacking with interactive flippable How To Solder comic book by Mitch Altman
** LED namebadge circuit hacking project
* 3D printer (currently loops prints of Bre Pettis' head).
* Thread hacking (Poster shows wearable merit badge project that we could simulate)

OTHER HACKERSPACES IN VR
--------------------------
* [Sudoroom Matterport scan](https://my.matterport.com/webgl_player/?m=9JVcHS4y7aY&utm_source=4) donated by [Immers.io](http://www.immers.io)
* [Glider.ink](http://glider.ink) is a graphic novel in development by Pawel Ngei about realistically portrayed hackers featuring hackerspaces [artistically based on VR hackerspace models](https://wiki.glider.ink/project/artstyle/moodboards) likE Simbridge.
Simbridge is open to being hacked on and added to by any Noisebridge participant or remixed to make parts of it useful for simulating other hackerspaces!

HELPING WITH 3D SCANNING
-------
Anyone can help with just a camera to take lots of pictures of rooms and objects at Noisebridge.
* Pictures can be combined into usable 3D models using [Zephyr Free](https://www.3dflow.net/3df-zephyr-free/) photogrammetry software or others.

HELPING WITH UNITY DEVELOPMENT
------
* Simbridge is developed with [Unity](http://unity3d.com), a free game engine available for Mac/Windows/Linux that can build for 21 platforms including HTML5 WebGL.

# Install Unity
# Clone the repo to try it or forki the repo to start your own version. 

WAYS TO CONTRIBUTE
--------------------
If you like Simbridge:
* improve and create new activity station prefabs
* improve on the Noisebridge space scene by capturing new details and adding them
* create new hackerspace scenes by forking the project and designing your level in Blender, Sketchup, Saber CSG, etc.
* introduce new people to hackerspaces by sharing SimBridge with them

DONATE
----------
* [DONATE to Nosiebridge and Simbridge project](Noisebridge: https://donate.noisebridge.net/projects/simbridge) and we will consense on what to use the money.
* Donations could help buy 3D scanning tools to scan all the props in the space.
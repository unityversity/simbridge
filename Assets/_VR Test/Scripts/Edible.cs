﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class Edible : VRTK_InteractableObject {

    public float eatDistance = 0.1f;
    public GameObject deactivateOnEat;
    public AudioSource audio;
    public AudioClip eatClip;
    public bool autoGrab = false;
    public bool eaten = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!eaten && IsGrabbed() || autoGrab)
        {
            if (Vector3.Distance(transform.position, Camera.main.transform.position) <= eatDistance)
            {
                Eat();
            }
        }
	}

    void Eat()
    {
        if (audio!=null)
        {
            audio.PlayOneShot(eatClip);
        }
        deactivateOnEat.SetActive(false);
    }
}

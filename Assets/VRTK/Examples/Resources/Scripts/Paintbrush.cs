﻿namespace VRTK.Examples
{
    using UnityEngine;

    public class Paintbrush : VRTK_InteractableObject
    {
        private GameObject paintPrefab;
        private float bulletSpeed = 0f;
        //private float bulletLife = 1000f;

        public override void StartUsing(GameObject usingObject)
        {
            base.StartUsing(usingObject);
            FirePaint();
        }

        #region MonoBehaviour

        protected void Start()
        {
            paintPrefab = transform.Find("PaintPrefab").gameObject;
            paintPrefab.SetActive(false);
        }

        private void Update()
        {
            if (IsUsing())
            {
                FirePaint();
            }
        }

        #endregion MonoBehaviour

        private void FirePaint()
        {
            GameObject paintClone = Instantiate(paintPrefab, paintPrefab.transform.position, paintPrefab.transform.rotation) as GameObject;
            paintClone.SetActive(true);
            //Rigidbody rb = paintClone.GetComponent<Rigidbody>();
            //rb.AddForce(-paintPrefab.transform.forward * bulletSpeed);
            //Destroy(paintClone, bulletLife);
        }
    }
}
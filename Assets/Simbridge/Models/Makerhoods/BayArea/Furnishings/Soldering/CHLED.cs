﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Blinks LEDs between a set color and black when off using material and lights
/// </summary>
public class CHLED : MonoBehaviour {

	public bool on = false;
	public Light light;
    public GameObject emitterGO;
    public Renderer emitterRenderer;
	public Material emitterMaterial;
    public Color emissionColor = Color.red;
    public string emitterMeshName = "Mesh1";
    public bool blink = false;
	public float blinkRate = 0.01f;
	public float lastBlink = 0f;
    public float minBlinkRate = 0.05f;
    public float maxBlinkRate = 0.1f;

    public bool randomizeRate = true;
    public bool randomizeColor = true;
    public bool randomizeColorOnBlink = true;
    public bool turnOffOnBlink = false;

    public Color RandomColor()
    {
        return new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1f);
    }
	// Use this for initialization
	void Start () {
        if (randomizeRate)
        {
            blinkRate = Random.Range(minBlinkRate, maxBlinkRate);
        }
        if (randomizeColor)
        {
            emissionColor = RandomColor();
        }

		light = GetComponentInChildren<Light>();
		Renderer[] renderers = GetComponentsInChildren<Renderer>();
        if (emitterMaterial == null)
        {
            if (emitterMeshName != "")
            {
                foreach (Renderer r in renderers)
                {
                    if (r.name == emitterMeshName)
                    {
                        emitterRenderer = r;
                        emitterMaterial = emitterRenderer.materials[1];
                    }
                }
            }
            else
            {
                if (emitterRenderer == null)
                {
                    if (emitterMeshName != "")
                    {
                        foreach (Renderer r in renderers)
                        {
                            emitterRenderer = r;
                        }
                    }
                }
                emitterMaterial = emitterRenderer.material;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (on)
		{
			emitterMaterial.color = emissionColor;
            emitterMaterial.SetColor("_EmissionColor", emissionColor);
			light.enabled = true;
            light.color = emissionColor;
		}
		else
		{
            if (turnOffOnBlink)
            {
                emitterMaterial.color = Color.black;
                light.enabled = false;
            }
            else if (randomizeColorOnBlink)
            {
                emitterMaterial.color = emissionColor;
                emitterMaterial.SetColor("_EmissionColor", emissionColor);
                light.enabled = true;
                light.color = emissionColor;
            }
		}
		if(blink && (Time.time - lastBlink > blinkRate))
		{
			on = !on;
			lastBlink = Time.time;
            emissionColor = RandomColor();
		}
	}
}

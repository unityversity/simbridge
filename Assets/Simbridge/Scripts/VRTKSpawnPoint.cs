﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Sets player spawnpoint for multiple SDK setups and supports respawning and restarting.
/// </summary>
public class VRTKSpawnPoint : MonoBehaviour {
    public bool found = false;
    public bool spawned = false;
    public string playerGOTagString = "Player";
    public List<GameObject> potentialPlayerGOs = new List<GameObject>();
    public GameObject playerGO;
    public string simulatorGOName = "VRSimulatorCameraRig";
    public string steamvrGOName = "[CameraRig]";
    public bool reloadKey = true;
    public KeyCode reloadKeyCode = KeyCode.Backspace;


    public void FindPlayer()
    {
        if (!found)
        {
            playerGO = GameObject.FindWithTag("Player");
            if (playerGO != null) found = true;
        }
        if (!found)
        {
            playerGO = GameObject.Find(simulatorGOName);
            if (playerGO != null) found = true;
        }
        if (!found)
        {
            playerGO = GameObject.Find(steamvrGOName);
            if (playerGO != null) found = true;
        }
        if (potentialPlayerGOs.Count == 0)
        {
            //
        }
    }

    public void RespawnPlayer()
    {
        playerGO.transform.position = transform.position;
        spawned = true;
    }

    public void ReloadLevel()
    {
        Debug.Log("Reloading");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

    public void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    // Use this for initialization
    void Start () {
        LockCursor();
        GetComponent<Renderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (!found)
        {
            FindPlayer();
        }
        else
        {
            if (!spawned)
            {
                RespawnPlayer();
            }
        }
        if (reloadKey)
        {
            if (Input.GetKeyDown(reloadKeyCode))
            {
                ReloadLevel();
            }
        }
	}
}
